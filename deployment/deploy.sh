#!/bin/bash

# This shell script requires to have property files and log4j files of all environments to be present in the directory.
# And it needs to be run with root privileges It assumes that you are running the script as the user on which you want the service to be running.

# -e : exit immediately when a command fails
# -E : ERR trap will be inherited by shell functions
set -Ee

# takes the environment as argument. valid arguments are exp, dev, prod
if [ -z "$1" ]; then
  ENV=exp
else
  ENV=$1
fi
echo "--> Setting current environment is $ENV"

cd $(dirname "$0")

# define the paths required
WORKING_DIR=/var/lib/allego-refract-consumer
BACKUPS_DIR=$WORKING_DIR/backups
CURRENT_DIR=$(pwd)
echo "--> Using the following directories-> working directory: $WORKING_DIR, current directory: $CURRENT_DIR "

# copy jar and config files for specified environment to working directory.
# NOTE: this will overwrite existing files.
echo "--> Copying application files to working directory"
cp -rf $CURRENT_DIR/log4j2-$ENV.xml $WORKING_DIR/log4j2.xml
cp -rf $CURRENT_DIR/application-$ENV.yml $WORKING_DIR/application.yml
cp -rf $CURRENT_DIR/allego-refract-consumer.jar $WORKING_DIR/allego-refract-consumer.jar

# Take a backup of current .zip file.
mkdir -p $BACKUPS_DIR
echo "--> Creating a backup of $CURRENT_DIR in $BACKUPS_DIR"
cp -rf $CURRENT_DIR/allegorefractconsumer-*.zip $BACKUPS_DIR

# remove old backups
cd $BACKUPS_DIR
ls -t | tail -n +4 | xargs -r rm -rf --

# make allego user owner
echo "--> Setting up allego user as owner of $WORKING_DIR"
chown -Rf allego:allego $WORKING_DIR

# start the service
echo "--> Restarting Allego Refract Consumer application service"
systemctl restart allego-refract-consumer.service

echo "==> deployment done. Status: "

echo $(systemctl status allego-refract-consumer)

