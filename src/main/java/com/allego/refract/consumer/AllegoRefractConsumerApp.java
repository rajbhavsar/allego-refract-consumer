/*
 * ===========================================================================
 * Copyright 2021, Allego Corporation, MA USA
 *
 * This file and its contents are proprietary and confidential to and the sole
 * intellectual property of Allego Corporation.  Any use, reproduction,
 * redistribution or modification of this file is prohibited except as
 * explicitly defined by written license agreement with Allego Corporation.
 * ===========================================================================
 */

package com.allego.refract.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AllegoRefractConsumerApp
{
	public static void main(String[] args)
	{
		SpringApplication.run(AllegoRefractConsumerApp.class, args);
	}
}