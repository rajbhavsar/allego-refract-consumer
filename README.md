# Allego Refract Consumer

## 1. Deployment

### 1.1. Prerequisites

- Git, Java 11 pre-installed
- 500 - 1Gb available RAM.
- MySql Database server connectivity.
- ActiveMQ instance/cluster is running
- AWS CLI is setup with Allego's credentials, read/write access to s3 bucket

### 1.2. Set up steps.

- Create a directory `"allego-refract-consumer"` at `/var/lib/`. Make sure user `allego` has ownership of this directory. Underneath this directory create another directory `"backups"` to store backup files.

- Copy file `deployment/allego-refract-consumer_{ENVIRONMENT}` to `/lib/systemd/system` directory and rename it to `allego-refract-consumer.service`.

- If not already exist then create directories `/var/log/allego` and make sure user `allego` has ownership and all permissions of `allego` directory.

- Enable the service to auto start
    ```
     sudo systemctl daemon-reload; sudo systemctl enable allego-refract-consumer.service
    ```

### 1.3. Steps to deploy

1. Execute below command from source code. This will generate file `allegorefractconsumer-{commitId}.zip` in `/dist` directory.
    ```
     ./gradlew dist
    ```
2. Upload the zip file to s3
    ```
     ./gradlew uploadS3
    ``` 
3. Download the `.zip` file under the `/tmp/allegorefractconsumer` directory on the targeted machine and Unzip it using the below command.
    ```
     unzip {zip-file-name}.zip -d {zip-file-name}
    ```
    Note: make sure you unzip the file under a directory that has the same name as the name of zip file.

4. Find the `deploy.sh` file within the directory and execute it using below command.
    ```
      sudo sh deploy.sh exp
    ```
    Note: The `deploy.sh` takes the environment as an argument. valid arguments are exp, dev, prod.

5. Now the application should be running, To check if the application is running,
    - Hit the health endpoint. `http://<vm>:8011/actuator/health`